import { Injectable } from '@angular/core';

import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler
} from '@angular/common/http';
import { LoaderService } from '../loader.service';
import { finalize } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoaderInterceptor {

    constructor(private loader: LoaderService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        this.loader.show();
        const authToken = `thinKart${new Date().getTime()}`;
        req = req.clone({ setHeaders: { 'auth-header': authToken } });
        return next.handle(req).pipe(finalize(() => this.loader.hide()));
    }
}


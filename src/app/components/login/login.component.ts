import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { LoaderService } from '../../services/loader.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor(private fb: FormBuilder, private loader: LoaderService, private authService: AuthService, private router: Router) { }

    loginForm: FormGroup;
    unSubscribe$: Subject<boolean> = new Subject<boolean>();
    isLoading: Subject<boolean> = this.loader.isLoading;

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            username: ["", Validators.required],
            password: ["", Validators.required],
        });
    }

    checkLogin() {
        this.authService.validateUser()
            .pipe(takeUntil(this.unSubscribe$))
            .subscribe((data) => {
                if (data["checkUser"].username === "admin" && data["checkUser"].password === "admin") {
                    this.router.navigateByUrl("/auth/admin");
                } else {
                    this.router.navigateByUrl("/");
                }
            }, (err) => {

            })
    }

}

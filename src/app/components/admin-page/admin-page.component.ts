import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-admin-page',
    templateUrl: './admin-page.component.html',
    styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

    constructor(private http: HttpClient, private fb: FormBuilder) { }

    addItemForm: FormGroup;
    updateItemForm: FormGroup;
    deleteItemForm: FormGroup;
    unSubscribe$: Subject<boolean> = new Subject<boolean>();

    public categoryFilter: string;
    public product_details = {};
    public productNameList = [];
    public product_list = [];
    public productType = [];
    public isProductPanelShow: Boolean = true;
    public isAddPanelShow: Boolean = false;
    public isUpdatePanelShow: Boolean = false;
    public isDeletePanelShow: Boolean = false;
    public isClothes: Boolean = false;
    public isSoldOut: Boolean = true;
    public CategoryList = { "clothes": "Clothes", "mobile": "Mobile" };
    public brandList = ["Arrow", "Spunk", "Polo"];


    ngOnInit(): void {
        this.addItemForm = this.fb.group({
            category: ["", Validators.required],
            productName: ["", Validators.required],
            productType: ["", Validators.required],
            brand: ["arrow", Validators.required],
            size: [0, Validators.required],
            stock: ["", Validators.required],
            price: ["", Validators.required],
        });

        this.updateItemForm = this.fb.group({
            category: ["", Validators.required],
            productName: ["", Validators.required],
            productType: ["", Validators.required],
            updateType: ["", Validators.required],
            size: [0, Validators.required],
            stock: [0, Validators.required],
            price: [0, Validators.required],
        });

        this.deleteItemForm = this.fb.group({
            category: ["", Validators.required],
            productType: ["", Validators.required],
            productName: ["", Validators.required]
        });

        this.http.get('./assets/data/thinKart.json')
            .pipe(takeUntil(this.unSubscribe$))
            .subscribe((data) => {
                this.product_details = data;
                this.product_list = this.product_details["product_list"];
            }, (err) => {
                console.log(err);
            })
    }

    showPanels(currentPanel) {
        this.isProductPanelShow = false;
        this.isAddPanelShow = false;
        this.isUpdatePanelShow = false;
        this.isDeletePanelShow = false;
        this[`is${currentPanel}PanelShow`] = true;

    }

    setCategory(e) {
        this.isClothes = e === 'clothes' ? true : false;
        this.productType = [...this.product_details["product_category"][e]];
        this.productNameList = this.product_details["product_list"].filter(obj => {
            return obj["category"] === e;
        });
    }

    getProductName(e) {
        let productObj=[];
        productObj = this.productNameList.filter(obj => {
            return obj["productType"] === e;
        });
        this.productNameList = productObj;
    }

    setUpdateType(e) {
        this.isSoldOut = (e.target.id === 'sold') ? true : false;
    }

    addNewItem() {
        //Api Call;
        //    this.http.get('./assets/data/thinKart.json')
        //     .pipe(takeUntil(this.unSubscribe$))
        //         .subscribe( (data) => {
        //
        //         }, (err) => {
        //             console.log(err);
        //         })
        /* Dummy Add Item */
        const params = this.addItemForm.value;
        params["item_latest"] = "added";
        this.product_list.push(params);
        this.showPanels("Product");

    }

    updateItem() {
         //Api Call;
        //    this.http.get('./assets/data/thinKart.json')
        //     .pipe(takeUntil(this.unSubscribe$))
        //         .subscribe( (data) => {
        //
        //         }, (err) => {
        //             console.log(err);
        //         })
        /* Dummy Update Item */
        console.log(this.updateItemForm.value)
        const productName= this.updateItemForm["controls"].productName.value;
        this.product_details["product_list"].find(obj => {
            if(obj.productName === productName) {
                if(this.updateItemForm["controls"].updateType.value === "update"){
                    obj.stock = this.updateItemForm['controls'].stock.value;
                    obj.price = this.updateItemForm['controls'].price.value;
                    obj["item_latest"] = "updated";
                } else {
                    obj.stock = 0;
                    obj["item_latest"] = "sold";
                }
            }
        });
        this.showPanels("Product");
    }

    deleteItem() {
        //Api Call;
        //    this.http.get('./assets/data/thinKart.json')
        //     .pipe(takeUntil(this.unSubscribe$))
        //         .subscribe( (data) => {
        //
        //         }, (err) => {
        //             console.log(err);
        //         })
        /* Dummy Update Item */
        const productName= this.deleteItemForm["controls"].productName.value;
        let index = this.product_list.findIndex(obj => obj.productName === productName);
        this.product_details["product_list"].splice(index,1);
        this.showPanels("Product");
    }

    applyFilter(filterKey, filterValue) {
        this.product_list = this.product_details["product_list"].filter(obj => {
            return obj[filterKey] === filterValue;
        });
    }



}
